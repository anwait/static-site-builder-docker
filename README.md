# Static Site Builder Docker
## Table of Contents
- [About](#about)
- [Tools](#tools)
- [Instructions](#instructions)
- [Is it any good?](#is-it-any-good)


## About
This is a docker container in order to build my static sites which at this point are mostly done with nodejs and or jekyll which means that both dependencies are met, additionally yarn is installed because I said so.


## Tools
- [Yarn](https://yarnpkg.com/)
- [Bundler](https://bundler.io/)


## Instructions
### Usage
You have to use the actual commands yourself but at least the hard dependencies should be met at this point.

```sh
docker run -u $(id -u):$(id -g) -v $(pwd):/workdir anwait/staticsitebuilder
```


### Building
In order to build this image you have to run the following command:

```sh
docker build -t anwait/staticsitebuilder .
```


### Uploading
```sh
docker push anwait/staticsitebuilder
```


## Is it any good?
Yes!