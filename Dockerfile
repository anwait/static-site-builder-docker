FROM alpine:3.15

LABEL maintainer="Anatoli Wagner <info@anwait.org>"

RUN apk add --no-cache --upgrade \
  # Core dependencies
  ruby \
  nodejs \
  yarn \
  # Submodules for the lazy ones
  git \
  openssh \
  # Needed in order to build native extensions
  build-base \
  ruby-dev \
  # Bundler package ready to go
  ruby-bundler \
  # Hidden Jekyll dependencies
  ruby-json \
  ruby-bigdecimal \
  # Set unprivileged user
  && adduser \
  -h /workdir \
  -s /bin/sh \
  -D \
  -u 1000 \
  sitebuilder \
  # Configure gem user install
  && echo "gem: --user-install" > /etc/gemrc

USER sitebuilder

# Configure path variable
RUN bundler \
  config \
  path \
  vendor/bundle

WORKDIR /workdir

VOLUME ["/workdir"]

ENTRYPOINT ["/bin/sh", "-c"]
